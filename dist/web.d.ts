import { QueueService } from ".";
import { ModelViewService } from "./services/model-view-service";
import { UiService } from "./services/ui-service";
export declare namespace Web {
    var modelViewService: ModelViewService;
    var queueService: QueueService;
    var uiService: UiService;
    function init(app: any): void;
}
