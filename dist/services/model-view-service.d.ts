import { ModelView } from "../model-view";
import { UiService } from "./ui-service";
declare class ModelViewService {
    private uiService;
    constructor(uiService: UiService);
    resolve(resolve: any, controller_promise: Promise<ModelView>): Promise<void>;
}
export { ModelViewService };
