"use strict";
exports.__esModule = true;
var UiService = /** @class */ (function () {
    function UiService(app) {
        this.app = app;
    }
    UiService.prototype.navigate = function (url, reloadCurrent, ignoreCache) {
        if (reloadCurrent === void 0) { reloadCurrent = true; }
        if (ignoreCache === void 0) { ignoreCache = false; }
        this.app.view.main.router.navigate(url, {
            reloadCurrent: reloadCurrent,
            ignoreCache: ignoreCache
        });
    };
    UiService.prototype.showExceptionPopup = function (ex) {
        if (ex.name == "IpfsException") {
            this.app.dialog.alert(ex.message, "Problem connecting to IPFS");
        }
        else {
            this.app.dialog.alert(ex.message, "There was an error");
        }
    };
    UiService.prototype.showPopup = function (message) {
        this.app.dialog.alert(message);
    };
    // async loadComponentState(component, showSpinner = true) {
    //     if (showSpinner) this.app.preloader.show()
    //     let context = component.$route.context
    //     //Get promise from component and await it. Then set the state to the result.
    //     if (context) {
    //         let model = await context.fn()
    //         component.$setState(model)
    //     }
    //     if (showSpinner) this.app.preloader.hide()
    // }
    UiService.prototype.showSpinner = function () {
        this.app.preloader.show();
    };
    UiService.prototype.hideSpinner = function () {
        this.app.preloader.hide();
    };
    return UiService;
}());
exports.UiService = UiService;
