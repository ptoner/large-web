declare class UiService {
    app: any;
    constructor(app: any);
    navigate(url: string, reloadCurrent?: boolean, ignoreCache?: boolean): void;
    showExceptionPopup(ex: any): void;
    showPopup(message: any): void;
    showSpinner(): void;
    hideSpinner(): void;
}
export { UiService };
