"use strict";
exports.__esModule = true;
var _1 = require(".");
var model_view_service_1 = require("./services/model-view-service");
var ui_service_1 = require("./services/ui-service");
var Web;
(function (Web) {
    function init(app) {
        Web.uiService = new ui_service_1.UiService(app);
        Web.modelViewService = new model_view_service_1.ModelViewService(Web.uiService);
        Web.queueService = new _1.QueueService();
    }
    Web.init = init;
})(Web = exports.Web || (exports.Web = {}));
