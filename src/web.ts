import { QueueService, ModelView } from ".";
import { ModelViewService } from "./services/model-view-service"
import { UiService } from "./services/ui-service";


export namespace Web {
    export var modelViewService:ModelViewService
    export var queueService:QueueService
    export var uiService:UiService

    export function init(app) {
        Web.uiService = new UiService(app)
        Web.modelViewService = new ModelViewService(Web.uiService)
        Web.queueService = new QueueService()

    }
}