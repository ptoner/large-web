import { ModelView } from "../model-view"
import { Web } from "../web"
import { UiService } from "./ui-service";



class ModelViewService {

  constructor(
    private uiService:UiService
  ) {}

  //Handles routing to a controller
  async resolve(resolve, controller_promise: Promise<ModelView>) {

    let modelView: ModelView = await controller_promise;

    if (!modelView) return

    this.uiService.showSpinner()

    let ctx = await modelView.model

    let context = await ctx()

    this.uiService.hideSpinner()

    resolve({
      componentUrl: modelView.view
    },
      {
        context: context
      })

  }


}

export { ModelViewService }
